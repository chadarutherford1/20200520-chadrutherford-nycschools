//
//  NYCSchoolsModelTests.swift
//  20200520-ChadRutherford-NYCSchoolsTests
//
//  Created by Chad Rutherford on 5/20/21.
//

import XCTest
@testable import _0200520_ChadRutherford_NYCSchools

class NYCSchoolsModelTests: XCTestCase {

    func testSchoolsModelDataDecodesProperly() throws {
        // Given this data
        let jsonData = schoolsJSON

        // When it's decoded
        let decoder = JSONDecoder()
        let schools = try decoder.decode([NYCSchool].self, from: jsonData)

        // The array of schools should have items (should not be count of zero)
        XCTAssertNotEqual(schools.count, 0)
        // The dbn of the first school should be 02M260
        XCTAssertEqual(schools.first?.dbn, "02M260", "The dbn of the selected school should be '02M260'")
        // The school name of the first school should be Clinton School Writers & Artists, M.S. 260
        XCTAssertEqual(schools.first?.schoolName, "Clinton School Writers & Artists, M.S. 260", "The name of the selected school should be 'Clinton School Writers & Artists, M.S. 260'")
        // The city of the first school should be Manhattan
        XCTAssertEqual(schools.first?.city, "Manhattan", "The city of the selected school should be 'Manhattan'")
    }
}
