//
//  SATScoresModelTests.swift
//  20200520-ChadRutherford-NYCSchoolsTests
//
//  Created by Chad Rutherford on 5/20/21.
//

import XCTest
@testable import _0200520_ChadRutherford_NYCSchools

class SATScoresModelTests: XCTestCase {

    func testSATScoresModelDataDecodesProperly() throws {
        // Given this data
        let jsonData = satScoresJSON

        // When it's decoded
        let decoder = JSONDecoder()
        let scores = try decoder.decode([SATScore].self, from: jsonData)

        // The array of scores should have items (should not be count of zero)
        XCTAssertNotEqual(scores.count, 0)
        // The dbn of the firwst school should be "01M292"
        XCTAssertEqual(scores.first?.dbn, "01M292", "The dbn should be '01M292'")
        // The school name of the first score should be "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES"
        XCTAssertEqual(scores.first?.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES", "The school name should be 'HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES'")
        // The number of test takers should decode as a string and be able to be converted into an Int
        XCTAssertEqual(scores.first?.testTakersString, "29", "The number of test takers should be '29'")
        XCTAssertEqual(scores.first?.testTakers, 29, "The number of test takers should now be a number 29")
        // The reading score should decode as a string and be able to be converted into an Int
        XCTAssertEqual(scores.first?.readingScoreString, "355", "The reading score should be '355'")
        XCTAssertEqual(scores.first?.readingScore, 355, "The reading score should now be a number 355")
        // The math score should decode as a string and be able to be converted into an Int
        XCTAssertEqual(scores.first?.mathScoreString, "404", "The math score should be '404'")
        XCTAssertEqual(scores.first?.mathScore, 404, "The math score should now be a number 404")
        // The writing score should decode as a string and be able to be converted into an Int
        XCTAssertEqual(scores.first?.writingScoreString, "363", "The writing score should be '363'")
        XCTAssertEqual(scores.first?.writingScore, 363, "The writing score should now be a number 363")
    }
}
