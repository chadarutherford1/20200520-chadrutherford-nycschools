//
//  NetworkTests.swift
//  20200520-ChadRutherford-NYCSchoolsTests
//
//  Created by Chad Rutherford on 5/20/21.
//

import XCTest
@testable import _0200520_ChadRutherford_NYCSchools

class NetworkTests: XCTestCase {

    var possibleSchool: NYCSchool?
    var possibleError: Error?

    func testAppropriateRequestSucceeds() {
        // Given valid data and a valid response
        let mockLoader = MockNetworkController(
            data: validData,
            response: validResponse,
            error: nil
        )

        // When a network call is made
        networkCall(mockLoader) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .success(let school):
                self.possibleSchool = school
            case .failure(let error):
                self.possibleError = error
            }

            // School should have a value and error should be nil
            XCTAssertNotNil(self.possibleSchool)
            XCTAssertNil(self.possibleError)
        }
    }

    func testNetworkWithErrorFails() {
        // Given a valid error with no response or data
        let mockError = MockNetworkController(
            data: nil,
            response: nil,
            error: validError
        )

        // When a network call is made
        networkCall(mockError) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let school):
                self.possibleSchool = school
            case .failure(let error):
                self.possibleError = error
            }
            // School should be nil and the call should fail with an error
            XCTAssertNil(self.possibleSchool)
            XCTAssertNotNil(self.possibleError)
        }
    }

    func testPossibleDeveloperErrorFails() {
        // Given invalid data and an invalid response
        let mockError = MockNetworkController(
            data: invalidData,
            response: invalidResponse,
            error: nil
        )

        // When a network call is made
        networkCall(mockError) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let school):
                self.possibleSchool = school
            case .failure(let error):
                self.possibleError = error
            }
            // School should be nil and the call should fail with an error
            XCTAssertNil(self.possibleSchool)
            XCTAssertNotNil(self.possibleError)
        }
    }

    func testServerErrorFails() {
        // Given the following error scenarios
        let errorScenarios = [
            MockNetworkController(data: invalidData, response: validResponse, error: nil), // Returns status code 200 with invalid data
            MockNetworkController(data: nil, response: validResponse, error: nil), // Returns status code 200 but missing data
            MockNetworkController(data: validData, response: invalidResponse, error: nil), // Returns status code 300+ but contains valid data
            MockNetworkController(data: nil, response: invalidResponse, error: validError), // Returns status code similar to  302 (e.g. too many redirects)
            MockNetworkController(data: nil, response: invalidResponse, error: nil), // Returns status code 500 (Server Error)
        ]

        for scenario in errorScenarios {
            // When network calls are made for each scenario
            networkCall(scenario) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let school):
                    self.possibleSchool = school
                case .failure(let error):
                    self.possibleError = error
                }
                // School should be nil and the call should fail with an error
                XCTAssertNil(self.possibleSchool)
                XCTAssertNotNil(self.possibleError)
            }
        }
    }

    func testMissingResponseFails() {
        // Given the following error scenarios
        let errorScenarios = [
            MockNetworkController(data: invalidData, response: nil, error: nil), // No HTTP response returned.
            MockNetworkController(data: validData, response: nil, error: nil), // No HTTP response returned.
            MockNetworkController(data: validData, response: nil, error: validError), // No HTTP response returned.
            MockNetworkController(data: invalidData, response: nil, error: validError), // Approximation of NSURLErrorBadServerResponse returned.
        ]

        for scenario in errorScenarios {
            // When a network call is made for each scenario
            networkCall(scenario) { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success(let school):
                    self.possibleSchool = school
                case .failure(let error):
                    self.possibleError = error
                }
                // School should be nil and the call should fail with an error
                XCTAssertNil(self.possibleSchool)
                XCTAssertNotNil(self.possibleError)
            }
        }
    }

    override func tearDownWithError() throws {
        possibleSchool = nil
        possibleError = nil
    }
}

extension NetworkTests {
    var validData: Data {
        let object = [
            "dbn": "02M260",
            "school_name": "Clinton School Writers & Artists, M.S. 260",
            "city": "Manhattan",
        ]
        return try! JSONEncoder().encode(object)
    }

    var invalidData: Data {
        let object = [
            "dbn": 3.14,
            "school_name": 505050,
            "city": 21345,
        ]
        return try! JSONEncoder().encode(object)
    }

    var validResponse: URLResponse {
        return HTTPURLResponse(
            url: URL(string: APIEndpoints.schoolsJSON.rawValue)!,
            statusCode: 200,
            httpVersion: nil,
            headerFields: nil
        )!
    }

    var invalidResponse: URLResponse {
        HTTPURLResponse(
            url: URL(string: APIEndpoints.schoolsJSON.rawValue)!,
            statusCode: 500,
            httpVersion: nil,
            headerFields: nil
        )!
    }

    var validError: Error {
        return NSError(
            domain: "20200520-ChadRutherford-NYCSchoolsApp",
            code: -1,
            userInfo: nil
        )
    }

    private func networkCall(_ controller: NetworkLoader,
                             completion: @escaping (Result<NYCSchool, Error>) -> Void) {
        let expectation = self.expectation(description: "Waiting...")
        let network = APIController(network: controller)
        network.fetch(with: URL(string: APIEndpoints.schoolsJSON.rawValue)!) { (result: Result<NYCSchool, APIError>) in
            switch result {
            case .success(let receivedSchool):
                completion(.success(receivedSchool))
            case .failure(let fetchError):
                completion(.failure(fetchError))
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 2) { _ in

        }
    }
}
