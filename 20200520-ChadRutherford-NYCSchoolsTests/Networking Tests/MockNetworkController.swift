//
//  MockNetworkController.swift
//  20200520-ChadRutherford-NYCSchoolsTests
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation
@testable import _0200520_ChadRutherford_NYCSchools

final class MockNetworkController: NetworkLoader {

    let data: Data?
    let response: URLResponse?
    let error: Error?

    init(data: Data?, response: URLResponse?, error: Error?) {
        self.data = data
        self.response = response
        self.error = error
    }

    func get(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        DispatchQueue.global().asyncAfter(deadline: .now() + 1) {
            completion(self.data, self.response, self.error)
        }
    }
}
