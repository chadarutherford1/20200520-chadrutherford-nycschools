//
//  SATScoreViewModel.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

final class SATScoreViewModel {

    // MARK: - Properties
    /// Binding property for the view model to update the table view when the data is loaded
    var score: Binding<SATScore?> = Binding(nil)
    // The API Controller responsible for fetching the network data
    let apiController = APIController()
    var school: NYCSchool

    // MARK: - Initialization
    init?(with school: NYCSchool?) {
        guard let school = school else { return nil }
        self.school = school
        fetchScores()
    }

    // MARK: - Fetch method for the score property
    func fetchScores() {
        guard let url = URL(string: APIEndpoints.satJSON.rawValue) else { return }
        apiController.fetch(with: url) { [weak self] (result: Result<[SATScore], APIError>) in
            switch result {
            case .success(let scores):
                guard let self = self,
                      let resultScore = scores.filter({ $0.dbn == self.school.dbn }).first else { return }
                self.score.value = resultScore
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
