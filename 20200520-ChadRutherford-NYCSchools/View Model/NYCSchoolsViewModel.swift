//
//  NYCSchoolsViewModel.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

final class NYCSchoolsViewModel {

    // MARK: - Properties
    /// Binding property for the view model to update the table view when the data is loaded
    var schools: Binding<[NYCSchool]> = Binding([])
    // The API Controller responsible for fetching the network data
    let apiController = APIController()

    // MARK: - Initialization
    init() {
        fetchSchools()
    }

    // MARK: - Fetch method for the schools property
    func fetchSchools() {
        guard let url = URL(string: APIEndpoints.schoolsJSON.rawValue) else { return }
        apiController.fetch(with: url) { (result: Result<[NYCSchool], APIError>) in
            switch result {
            case .success(let schools):
                self.schools.value = schools.sorted(by: { $0.schoolName < $1.schoolName })
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
