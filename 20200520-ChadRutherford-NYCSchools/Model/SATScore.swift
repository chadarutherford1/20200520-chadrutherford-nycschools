//
//  SATScore.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

/// Model Data for an SATScore Object
struct SATScore: Codable {

    // MARK: - Properties
    let dbn: String
    let schoolName: String
    let testTakersString: String
    let readingScoreString: String
    let mathScoreString: String
    let writingScoreString: String

    // MARK: - Computed Propertes
    var testTakers: Int {
        Int(testTakersString) ?? 0
    }

    var readingScore: Int {
        Int(readingScoreString) ?? 0
    }

    var mathScore: Int {
        Int(mathScoreString) ?? 0
    }

    var writingScore: Int {
        Int(writingScoreString) ?? 0
    }

    // MARK: - Coding Keys
    // Used for decoding JSON objects into Swift Objects
    enum SATScoreKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case testTakers = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }

    // MARK: - Initialization
    // Manual Codable Decoding
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: SATScoreKeys.self)
        dbn = try container.decode(String.self, forKey: .dbn)
        schoolName = try container.decode(String.self, forKey: .schoolName)
        testTakersString = try container.decode(String.self, forKey: .testTakers)
        readingScoreString = try container.decode(String.self, forKey: .readingScore)
        mathScoreString = try container.decode(String.self, forKey: .mathScore)
        writingScoreString = try container.decode(String.self, forKey: .writingScore)
    }
}
