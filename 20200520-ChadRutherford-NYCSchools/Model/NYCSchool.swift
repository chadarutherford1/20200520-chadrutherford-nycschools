//
//  NYCSchool.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation


/// Model Data for an NYCSchool Object
struct NYCSchool: Codable, Hashable {

    // MARK: - Properties
    let dbn: String
    let schoolName: String
    let city: String

    // MARK: - Coding Keys
    // Used for decoding JSON objects into Swift Objects
    enum NYCSchoolKeys: String, CodingKey {
        case dbn, city
        case schoolName = "school_name"
    }

    // MARK: - Initialization
    // Manual Codable Decoding
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: NYCSchoolKeys.self)
        dbn = try container.decode(String.self, forKey: .dbn)
        schoolName = try container.decode(String.self, forKey: .schoolName)
        city = try container.decode(String.self, forKey: .city)
    }
}
