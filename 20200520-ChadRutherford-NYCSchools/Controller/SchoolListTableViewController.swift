//
//  SchoolListTableViewController.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import UIKit

/// Class Responsible for displaying the school data in a list
final class SchoolListTableViewController: UITableViewController {
    // Type alias to make using the diffable data source snapshot morereadable
    typealias NYCSchoolSnapshot = NSDiffableDataSourceSnapshot<Section, NYCSchool>

    // The view model for the table view
    let nycSchoolsViewModel = NYCSchoolsViewModel()

    /// The data source for the table view (Utilizing the UITableViewDiffableDataSource)
    lazy var diffableDataSource: NYCSchoolsTableViewDataSource = {
        NYCSchoolsTableViewDataSource(tableView: tableView) { tableView, indexPath, school in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SchoolsCell.reuseIdentifier, for: indexPath) as? SchoolsCell else { fatalError("Unable to dequeue a SchoolsCell") }
            cell.school = school
            return cell
        }
    }()

    // MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = true
    }

    // MARK: - Configuration
    private func configureTableView() {
        view.backgroundColor = .systemBackground
        title = "NYC Schools"
        tableView.register(SchoolsCell.self, forCellReuseIdentifier: SchoolsCell.reuseIdentifier)
        tableView.dataSource = diffableDataSource
        guard isViewLoaded else { return }
        nycSchoolsViewModel.schools.bind { [weak self] _ in
            guard let self = self else { return }
            self.updateSnapshot()
        }
    }

    // MARK: - Private Methods
    private func updateSnapshot() {
        var snapshot = NYCSchoolSnapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(nycSchoolsViewModel.schools.value)
        diffableDataSource.apply(snapshot)
    }

    // MARK: - Instantiation
    static func makeSchoolListTableViewController() -> SchoolListTableViewController {
        let schoolListTableViewController = SchoolListTableViewController(nibName: nil, bundle: nil)
        return schoolListTableViewController
    }

    // MARK: - UITableViewDelegate Methods
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = nycSchoolsViewModel.schools.value[indexPath.row]
        let satScoresDetailViewController = SATScoresDetailViewController.makeSATScoresDetailViewController(with: school)
        navigationController?.pushViewController(satScoresDetailViewController, animated: true)
    }
}
