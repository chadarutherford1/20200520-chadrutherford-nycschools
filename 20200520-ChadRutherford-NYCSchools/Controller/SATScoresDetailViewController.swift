//
//  SATScoresDetailViewController.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import UIKit

final class SATScoresDetailViewController: UIViewController {

    // MARK: - Properties
    // The school property being passed in from the table view
    var school: NYCSchool?
    // The view model for the table view
    lazy var satScoreViewModel = SATScoreViewModel(with: school)

    // MARK: - UIElements
    let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .large
        return activityIndicator
    }()

    let numberOfTestTakersLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textColor = .label
        label.text = "Number of students:"
        return label
    }()

    let numberOfTestTakersValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.textColor = .secondaryLabel
        label.text = "---"
        return label
    }()

    let averageReadingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textColor = .label
        label.text = "Average Reading Score:"
        return label
    }()

    let averageReadingValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.textColor = .secondaryLabel
        label.text = "---"
        return label
    }()

    let averageMathLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textColor = .label
        label.text = "Average Math Score:"
        return label
    }()

    let averageMathValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.textColor = .secondaryLabel
        label.text = "---"
        return label
    }()

    let averageWritingLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textColor = .label
        label.text = "Average Writing Score:"
        return label
    }()

    let averageWritingValueLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.textColor = .secondaryLabel
        label.text = "---"
        return label
    }()

    // MARK: - View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        satScoreViewModel?.score.bind { [weak self] _  in
            guard let self = self else { return }
            self.activityIndicator.startAnimating()
            self.updateViews()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.prefersLargeTitles = false
    }

    // MARK: - Configuration of UI Elements
    private func configureView() {
        title = school?.schoolName
        view.backgroundColor = .systemBackground

        view.addSubview(activityIndicator)
        activityIndicator.center(in: view)

        view.addSubview(numberOfTestTakersLabel)
        view.addSubview(numberOfTestTakersValueLabel)
        numberOfTestTakersLabel.constrainToSafeArea(of: view, leading: 16, top: 40)
        numberOfTestTakersValueLabel.constrainToSafeArea(of: view, top: 40, trailing: 16)

        view.addSubview(averageReadingLabel)
        view.addSubview(averageReadingValueLabel)
        averageReadingLabel.constrainTopToBottomOf(view: numberOfTestTakersLabel, top: 16)
        averageReadingLabel.constrain(to: view, leading: 16)
        averageReadingValueLabel.constrainTopToBottomOf(view: numberOfTestTakersValueLabel, top: 16)
        averageReadingValueLabel.constrain(to: view, trailing: 16)
        averageReadingValueLabel.center(with: averageReadingLabel)

        view.addSubview(averageMathLabel)
        view.addSubview(averageMathValueLabel)
        averageMathLabel.constrainTopToBottomOf(view: averageReadingLabel, top: 16)
        averageMathLabel.constrain(to: view, leading: 16)
        averageMathValueLabel.constrainTopToBottomOf(view: averageReadingValueLabel, top: 16)
        averageMathValueLabel.constrain(to: view, trailing: 16)
        averageMathValueLabel.center(with: averageMathLabel)

        view.addSubview(averageWritingLabel)
        view.addSubview(averageWritingValueLabel)
        averageWritingLabel.constrainTopToBottomOf(view: averageMathLabel, top: 16)
        averageWritingLabel.constrain(to: view, leading: 16)
        averageWritingValueLabel.constrainTopToBottomOf(view: averageMathValueLabel, top: 16)
        averageWritingValueLabel.constrain(to: view, trailing: 16)
        averageWritingValueLabel.center(with: averageWritingLabel)
    }

    // MARK: - Updating of Data
    private func updateViews() {
        guard let score = satScoreViewModel?.score.value else { return }
        activityIndicator.stopAnimating()
        numberOfTestTakersValueLabel.text = "\(score.testTakers)"
        averageReadingValueLabel.text = "\(score.readingScore)"
        averageMathValueLabel.text = "\(score.mathScore)"
        averageWritingValueLabel.text = "\(score.writingScore)"
    }

    // MARK: - Instantiation
    static func makeSATScoresDetailViewController(with school: NYCSchool) -> SATScoresDetailViewController {
        let satScoresDetailViewController = SATScoresDetailViewController(nibName: nil, bundle: nil)
        satScoresDetailViewController.school = school
        return satScoresDetailViewController
    }
}
