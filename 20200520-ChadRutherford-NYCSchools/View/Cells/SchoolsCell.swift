//
//  SchoolsCell.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import UIKit

final class SchoolsCell: UITableViewCell {

    // MARK: - UI Elements
    let schoolNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textColor = .label
        return label
    }()

    let schoolCityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.textColor = .secondaryLabel
        return label
    }()

    // MARK: - Properties
    var school: NYCSchool? {
        didSet {
            updateViews()
        }
    }

    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
    }

    /// Initializer not implemented. Not required in a programmatic approach
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overridden Superclass Methods
    override func prepareForReuse() {
        schoolNameLabel.text = ""
        schoolCityLabel.text = ""
    }

    // MARK: - Private Methods
    private func configureCell() {
        addSubview(schoolNameLabel)
        addSubview(schoolCityLabel)
        schoolNameLabel.constrain(to: contentView, leading: 16, top: 8, trailing: 16)
        schoolCityLabel.constrainTrailingGreaterThanOrEqualTo(to: contentView, leading: 16, trailing: 16, bottom: 16)
        schoolCityLabel.constrainTopToBottomOf(view: schoolNameLabel, top: 8)
    }

    private func updateViews() {
        guard let school = school else { return }
        schoolNameLabel.text = school.schoolName
        schoolCityLabel.text = school.city
    }
}

// MARK: - Reuse Identifiable Protocol Conformance
extension SchoolsCell: ReuseIdentiaifble {
    static var reuseIdentifier: String {
        String(describing: Self.self)
    }
}
