//
//  NYCSchoolsTableViewDataSource.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import UIKit

/// This class is responsible for displaying the schools data in the table view
final class NYCSchoolsTableViewDataSource: UITableViewDiffableDataSource<Section, NYCSchool> {
    override init(tableView: UITableView, cellProvider: @escaping UITableViewDiffableDataSource<Section, NYCSchool>.CellProvider) {
        super.init(tableView: tableView, cellProvider: cellProvider)
    }
}
