//
//  Binding.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

/// This class is responsible for allowing objects to have a binding between them
/// Used in the viewModels to create a binding between them, and the respective view Controller
final class Binding<T> {

    typealias Listener = (T) -> Void
    var listener: Listener?

    var value: T {
        didSet {
            listener?(value)
        }
    }

    init(_ value: T) {
        self.value = value
    }

    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}
