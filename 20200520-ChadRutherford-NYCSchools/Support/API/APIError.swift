//
//  APIError.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

/// This enum is responsible for determining where the networking went wrong and displaying the correct information.
enum APIError: Error {
    case network(Error)
    case missingResponse
    case invalidResponse(Int)
    case invalidData
    case decodeError(Error)

    /// A localized Description property to display the error message
    var localizedDescription: String {
        switch self {
        case .network(let error):
            return "An unknown network error occurred: \(error)"
        case .missingResponse:
            return "The response object was missing"
        case .invalidResponse(let statusCode):
            return "The response returned from the server was invalid: \(statusCode)"
        case .invalidData:
            return "The data returned from the server was invalid."
        case .decodeError(let error):
            return "There was an error decoding the objects: \(error)"
        }
    }
}
