//
//  APIController.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

final class APIController {

    // MARK: - Properties
    let expectedResponseCodes = Set.init(200 ... 299)
    let network: NetworkLoader

    // MARK: - Initialization
    init(network: NetworkLoader = URLSession.shared) {
        self.network = network
    }

    // MARK: - Network Fetch
    func fetch<T: Codable>(with url: URL, completion: @escaping (Result<T, APIError>) -> Void) {
        let request = URLRequest(url: url)

        network.get(request: request) { possibleData, possibleResponse, possibleError in
            if let receivedError = possibleError {
                DispatchQueue.main.async {
                    completion(.failure(.network(receivedError)))
                }
                return
            }

            guard let response = possibleResponse as? HTTPURLResponse else {
                DispatchQueue.main.async {
                    completion(.failure(.missingResponse))
                }
                return
            }

            guard self.expectedResponseCodes.contains(response.statusCode) else {
                DispatchQueue.main.async {
                    completion(.failure(.invalidResponse(response.statusCode)))
                }
                return
            }

            guard let receivedData = possibleData else {
                DispatchQueue.main.async {
                    completion(.failure(.invalidData))
                }
                return
            }

            do {
                let decoder = JSONDecoder()
                let results = try decoder.decode(T.self, from: receivedData)
                DispatchQueue.main.async {
                    completion(.success(results))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(.decodeError(error)))
                }
            }
        }
    }
}
