//
//  APIEndpoints.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

/// This enum is responsible for encapsulating the endpoints in a more type safe way.
enum APIEndpoints: String {
    /// The endpoint utilized to make the network call to fetch the list of schools
    case schoolsJSON = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    /// The endpoint utilized to make the network call to fetch the SAT scores per school
    case satJSON = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}
