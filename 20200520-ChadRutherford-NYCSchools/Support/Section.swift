//
//  Section.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

// MARK: - Section Enum for Diffable Data Sources
enum Section {
    case main
}
