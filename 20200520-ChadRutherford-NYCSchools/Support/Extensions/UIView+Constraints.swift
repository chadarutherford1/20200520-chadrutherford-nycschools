//
//  UIView+Constraints.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import UIKit

extension UIView {

    /// Constrains the view to the specified view
    /// - Parameters:
    ///   - view: The view to be anchored to
    ///   - leading: Leading Constant
    ///   - top: Top Constant
    ///   - trailing: Trailing Constant
    ///   - bottom: Bottom Constant
    func constrain(to view: UIView, leading: CGFloat? = nil, top: CGFloat? = nil, trailing: CGFloat? = nil, bottom: CGFloat? = nil) {
        if let leadingConstant = leading {
            NSLayoutConstraint.activate([leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingConstant)])
        }
        if let topConstant = top {
            NSLayoutConstraint.activate([topAnchor.constraint(equalTo: view.topAnchor, constant: topConstant)])
        }
        if let trailingConstant = trailing {
            NSLayoutConstraint.activate([trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -trailingConstant)])
        }
        if let bottomConstant = bottom {
            NSLayoutConstraint.activate([bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -bottomConstant)])
        }
    }


    /// Constrains the view to the specified view (Trailing is greater than or equal to constant)
    /// - Parameters:
    ///   - view: The view to be anchored to
    ///   - leading: Leading Constant
    ///   - top: Top Constant
    ///   - trailing: Trailing Constant
    ///   - bottom: Bottom Constant
    func constrainTrailingGreaterThanOrEqualTo(to view: UIView, leading: CGFloat? = nil, top: CGFloat? = nil, trailing: CGFloat? = nil, bottom: CGFloat? = nil) {
        if let leadingConstant = leading {
            NSLayoutConstraint.activate([leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingConstant)])
        }
        if let topConstant = top {
            NSLayoutConstraint.activate([topAnchor.constraint(equalTo: view.topAnchor, constant: topConstant)])
        }
        if let trailingConstant = trailing {
            NSLayoutConstraint.activate([trailingAnchor.constraint(greaterThanOrEqualTo: view.trailingAnchor, constant: -trailingConstant)])
        }
        if let bottomConstant = bottom {
            NSLayoutConstraint.activate([bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -bottomConstant)])
        }
    }

    /// Constrains the view to the bottom of the specified view
    /// - Parameters:
    ///   - view: The view to be anchored to
    ///   - top: Top Constant
    func constrainTopToBottomOf(view: UIView, top: CGFloat) {
        NSLayoutConstraint.activate([topAnchor.constraint(equalTo: view.bottomAnchor, constant: top)])
    }

    /// Constrains the view to the specified view (Trailing is greater than or equal to constant)
    /// - Parameters:
    ///   - view: The view to be anchored to
    ///   - leading: Leading Constant
    ///   - top: Top Constant
    ///   - trailing: Trailing Constant
    ///   - bottom: Bottom Constant
    func constrainToSafeArea(of view: UIView, leading: CGFloat? = nil, top: CGFloat? = nil, trailing: CGFloat? = nil, bottom: CGFloat? = nil) {
        if let leadingConstant = leading {
            NSLayoutConstraint.activate([leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingConstant)])
        }
        if let topConstant = top {
            NSLayoutConstraint.activate([topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: topConstant)])
        }
        if let trailingConstant = trailing {
            NSLayoutConstraint.activate([trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -trailingConstant)])
        }
        if let bottomConstant = bottom {
            NSLayoutConstraint.activate([bottomAnchor.constraint(greaterThanOrEqualTo: view.bottomAnchor, constant: -bottomConstant)])
        }
    }

    /// Constrains the view to the center y anchor of the specified view
    /// - Parameter view: The view to be anchored to
    func center(with view: UIView) {
        NSLayoutConstraint.activate([
            centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }

    /// Constrains the view in the center of the specified view along the x and y axis
    /// - Parameter view: The view to be anchored to
    func center(in view: UIView) {
        NSLayoutConstraint.activate([
            centerYAnchor.constraint(equalTo: view.centerYAnchor),
            centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
}
