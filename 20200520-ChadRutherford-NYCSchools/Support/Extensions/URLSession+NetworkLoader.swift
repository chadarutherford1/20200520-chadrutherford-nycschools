//
//  URLSession+NetworkLoader.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

/// Extension on URLSession to allow networking and sawpping in a mock loader for testing purposes
extension URLSession: NetworkLoader {
    func get(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = dataTask(with: request, completionHandler: completion)
        task.resume()
    }
}
