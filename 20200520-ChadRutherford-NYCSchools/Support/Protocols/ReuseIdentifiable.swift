//
//  ReuseIdentifiable.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

/// This protocol enables callers to utilize the class name as a reuse identifier in a type safe way.
/// Less error prone than hard-coded strings.
protocol ReuseIdentiaifble: AnyObject {
    static var reuseIdentifier: String { get }
}
