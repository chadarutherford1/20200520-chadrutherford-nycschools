//
//  NetworkLoader.swift
//  20200520-ChadRutherford-NYCSchools
//
//  Created by Chad Rutherford on 5/20/21.
//

import Foundation

/// This protocol enables us to swap out normal networking functionality for a mock loader for testing purposes.
protocol NetworkLoader {
    func get(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
}
